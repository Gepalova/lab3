﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace lab1
{/// <summary>
/// 
/// </summary>
    public class Point
    {
        public double x;
        public double y;

        public Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public static Point operator +(Point p1, Point p2)
        {
            return new Point(p1.x + p2.x, p1.y + p2.y);
        }
        public Point MovePoint(Vector v)
        {
            return new Point(this.x + v.x, this.y + v.y);
        }
        public Point OppositPoint()
        {
            return new Point(this.x*(-1), this.y*(-1));
        }
    }
}